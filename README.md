# groot-recruiters-templates-example

- Contains the email templates

## Sinatra variables that you can use

- `@username`: The name of the sender of the email (a member of corporate)
- `@recruiter`: The recruiter object, containing the following attributes
  - @recruiter.first_name
  - @recruiter.last_name
  - @recruiter.company_name
